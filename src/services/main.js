import { HTTP } from "utils/http";
import * as ENDPOINTS from "./endpoints";

export default class MainApi {
	static getProducts = async () => {
		return HTTP.get(ENDPOINTS.FETCH_ALL_PRODUCTS);
	};

	static getProductWithID = async ({ productID }) => {
		return HTTP.post(ENDPOINTS.FETCH_PRODUCT_WITH_ID, { productID });
	};

	static addToCart = async ({ productID, userID }) => {
		return HTTP.post(ENDPOINTS.ADD_PRODUCT_TO_CART, { productID, userID });
	};

	static getCart = async ({ userID }) => {
		return HTTP.post(ENDPOINTS.FETCH_CART, { userID });
	};

	static removeFromCart = async ({ productID, userID }) => {
		return HTTP.post(ENDPOINTS.REMOVE_PRODUCT_FROM_CART, { productID, userID });
	};

	static payForCart = async ({ userID }) => {
		return HTTP.post(ENDPOINTS.PAY_FOR_CART, { userID });
	};

	static getOrder = async ({ userID }) => {
		return HTTP.post(ENDPOINTS.FETCH_ORDER, { userID });
	};
}
