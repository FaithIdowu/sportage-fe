import React from "react";
import { Route, Switch } from "react-router-dom";
import AllProducts from "components/AdminComponents/allProducts";
import AllOrders from "components/AdminComponents/allOrders";
import * as ROUTES from "routes";
class Admin extends React.Component {
	render() {
		return (
			<div className="app-container padded">
				<Switch>
					<Route exact path={ROUTES.ADMIN} component={AllProducts} />
					<Route exact path={ROUTES.ORDERS} component={AllOrders} />
				</Switch>
			</div>
		);
	}
}
export default Admin;
