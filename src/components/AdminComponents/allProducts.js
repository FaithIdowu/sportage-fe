import React from "react";
import { MainActions, AdminActions } from "actions";
import { DisplayCard, Loading } from "../Reusables";
import AddProduct from "./addProduct";
import { Button, Card, Header, Icon, Modal } from "semantic-ui-react";

class AllProducts extends React.Component {
	constructor() {
		super();

		this.state = {
			products: [],
			productsHaveLoaded: false,
			isLoading: false
		};
	}

	componentDidMount() {
		this.setState({ isLoading: true });
		this.getProducts();
	}

	handleClose = event => {
		event.preventDefault();
		this.setState({ isLoading: true });
		this.getProducts();
	};

	handleDelete = id => {
		this.setState({ isLoading: true });
		this.removeProduct(id);
	};

	async getProducts() {
		const products = await MainActions.getProducts();
		this.setState({ products, isLoading: false });
	}

	async removeProduct(id) {
		try {
			const productID = id;
			await AdminActions.removeProducts({ productID });
			this.getProducts();
		} catch (err) {
			throw err;
		}
	}

	render() {
		const { products, isLoading } = this.state;
		if (isLoading) {
			return <Loading />;
		}
		return (
			<React.Fragment>
				<h3>Sportage Products</h3>
				<Modal
					trigger={
						<Button size="big">
							<Icon name="plus" />
							&nbsp;New Product
						</Button>
					}
					size="small"
					onClose={this.handleClose}
				>
					<Modal.Header>Register a new Product</Modal.Header>
					<Modal.Content>
						<AddProduct />
					</Modal.Content>
				</Modal>
				{products.length > 0 ? (
					<div>
						<Card.Group stackable items centered>
							<DisplayCard
								productList={products}
								action={this.handleDelete}
								buttonText="Delete"
							/>
						</Card.Group>
					</div>
				) : (
					<div className="align-center flat">
						<Header size="huge">Products cannot be shown now</Header>
						<p>(There's probably something wrong with Faith.)</p>
					</div>
				)}
			</React.Fragment>
		);
	}
}

export default AllProducts;
