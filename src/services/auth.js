import { HTTP, generateBearer } from "utils/http";
import * as ENDPOINTS from "./endpoints";

export default class AuthenticationApi {
	static signin = async ({ email, password }) => {
		return HTTP.post(ENDPOINTS.USER_SIGNIN, { email, password });
	};

	static signup = async ({
		firstName,
		lastName,
		email,
		phoneNumber,
		address,
		password
	}) => {
		return HTTP.post(ENDPOINTS.SIGNUP, {
			firstName,
			lastName,
			email,
			phoneNumber,
			address,
			password
		});
	};

	static signout = async ({ email, refreshToken, token }) => {
		return HTTP.post(
			ENDPOINTS.SIGNOUT,
			{ email, refreshToken, token },
			generateBearer(token)
		);
	};
}
