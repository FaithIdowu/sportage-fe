import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faLock,
	faLockOpen,
	faShoePrints,
	faStamp,
	faHamburger
} from "@fortawesome/free-solid-svg-icons";

library.add(faHamburger, faLock, faLockOpen, faShoePrints, faStamp);
