const USER_TOKEN = "USER_TOKEN";
const USER_ID = "USER_ID";
const IS_ADMIN = "IS_ADMIN";
const CART = "CART";

class UserStorage {
	/**
	 * Sets an encoded user's authentication token to local storage.
	 */
	static set token(token) {
		sessionStorage.setItem(USER_TOKEN, btoa(token));
	}

	/**
	 * Gets the user's authentication token from local storage and decodes it.
	 */
	static get token() {
		const tk = sessionStorage.getItem(USER_TOKEN);
		return tk == null ? tk : atob(tk);
	}

	/**
	 * Sets the user ID to local storage.
	 */
	static set userID(id) {
		localStorage.setItem(USER_ID, btoa(id));
	}

	/**
	 * Sets the user info to local storage.
	 */
	static get userID() {
		const id = localStorage.getItem(USER_ID);
		return id != null ? atob(id) : id;
	}

	/**
	 * Creates a localStorage cart for users not logged in
	 */

	static createCart = product => {
		const cart = JSON.stringify(product);
		localStorage.setItem(CART, cart);
	};

	/**
	 * returns a localStorage cart for users not logged in
	 */
	static getCart = () => {
		const cart = localStorage.getItem(CART);
		return cart !== null ? JSON.parse(cart) : [];
	};

	/**
	 * Deletes the cart for users not logged in
	 */
	static clearCart = () => {
		localStorage.removeItem(CART);
	};

	/**
	 * Sets the admin status of user to local storage.
	 */
	static set isAdmin(is_admin) {
		sessionStorage.setItem(IS_ADMIN, btoa(is_admin));
	}

	/**
	 * Gets the admin status of user from local storage.
	 */
	static get isAdmin() {
		const is_admin = sessionStorage.getItem(IS_ADMIN);
		return is_admin == null ? is_admin : atob(is_admin);
	}
}

export default UserStorage;
