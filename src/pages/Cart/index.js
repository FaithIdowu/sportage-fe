import React from "react";
import CartComponent from "components/CartComponents";

class Cart extends React.Component {
	render() {
		return (
			<div className="app-container">
				<CartComponent {...this.props} />
			</div>
		);
	}
}

export default Cart;
