export const HOME = "/";

export const AUTH = "/auth";
export const SIGNUP = "/auth/register";
export const CART = "/cart";
export const ORDER = "/order";

export const ADMIN = "/admin";
export const ORDERS = "/admin/order";
