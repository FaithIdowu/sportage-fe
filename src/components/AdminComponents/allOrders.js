import React from "react";
import { AdminActions } from "actions";
import { OrderDisplayCard } from "../Reusables";

class AllOrders extends React.Component {
	constructor() {
		super();

		this.state = {
			orders: [],
			isLoading: false
		};
	}

	componentDidMount() {
		this.getOrders();
		this.setState({ isLoading: true });
	}

	async getOrders() {
		try {
			const list = await AdminActions.getAllOrder();
			this.setState({ orders: list, isLoading: false });
		} catch (err) {
			console.error(err);
		}
	}

	render() {
		const { orders } = this.state;
		return (
			<React.Fragment>
				{orders.length > 0 ? (
					<div className="padded">
						<h3>Customer Orders</h3>
						<OrderDisplayCard orderList={orders} />
					</div>
				) : (
					<div className="align-center flat padded empty-container">
						<div className="pt">
							<h3>Ogah, be like say business slow today</h3>
							<p>(There's probably something wrong with Ecommerce.)</p>
						</div>
					</div>
				)}
			</React.Fragment>
		);
	}
}

export default AllOrders;
