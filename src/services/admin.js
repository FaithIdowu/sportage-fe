import { HTTP, generateMultipartHeader, mergeHeaders } from "utils/http";
import * as ENDPOINTS from "./endpoints";
import { UserStorage } from "storage";

export default class AdminApi {
	static addProduct = async ({
		name,
		description,
		price,
		image,
		token = UserStorage.token
	}) => {
		const formData = new FormData();
		formData.append("name", name);
		formData.append("description", description);
		formData.append("price", price);
		formData.append("image", image);
		return HTTP.post(
			ENDPOINTS.ADD_PRODUCT,
			formData,
			mergeHeaders(generateMultipartHeader())
		);
	};

	static removeProduct = async ({ productID }) => {
		return HTTP.post(ENDPOINTS.REMOVE_PRODUCT, { productID });
	};

	static getAllOrder = async () => {
		return HTTP.get(ENDPOINTS.FETCH_ORDER);
	};
}
