import React from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import SignIn from "components/AuthComponents/Signin";
import SignUp from "components/AuthComponents/Signup";
import { isLoggedIn } from "utils/auth";
import { HOME, SIGNUP, AUTH } from "routes";

class Auth extends React.Component {
	render() {
		return isLoggedIn() ? (
			<Redirect to={HOME} />
		) : (
			<div className="app-container align-center">
				<div className="app-auth-content">
					<Switch>
						<Route exact path={AUTH} component={SignIn} />
						<Route exact path={SIGNUP} component={SignUp} />
					</Switch>
				</div>
			</div>
		);
	}
}

export default Auth;
