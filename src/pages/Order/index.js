import React from "react";
import OrderComponent from "components/OrderComponent";

export default () => (
	<div className="app-container">
		<OrderComponent />
	</div>
);
