import { AuthApi } from "services";
import { UserStorage } from "storage";
import { MainActions } from "actions";

class AuthActions {
	/**
	 * To sign in a user. After authentication
	 * the users token and ID are stored into web storage, and a pretend-hook is
	 * called to save whatever user has in localStorage cart to Database
	 */
	static async signin({ email, password }) {
		try {
			const { data } = await AuthApi.signin({ email, password });
			const { message, success, is_admin } = data;
			if (success) {
				UserStorage.token = data.token;
				UserStorage.isAdmin = is_admin;
				UserStorage.userID = data.user.id;
				this.transferCartToDB();
			}
			return { message, success };
		} catch (err) {
			throw err;
		}
	}

	/**
	 * To register a user. After authentication
	 * the users token and ID are stored into web storage, and a pretend-hook is
	 * called to save whatever user has in localStorage cart to Database
	 */
	static async signup({
		firstName,
		lastName,
		email,
		phoneNumber,
		address,
		password
	}) {
		try {
			const { data } = await AuthApi.signup({
				firstName,
				lastName,
				email,
				address,
				phoneNumber,
				password
			});
			const { message, success } = data;
			if (success) {
				UserStorage.token = data.token;
				UserStorage.userID = data.user.id;
				this.transferCartToDB();
			}
			return { message, success };
		} catch (err) {
			throw err;
		}
	}

	static async transferCartToDB() {
		const cart = UserStorage.getCart();
		if (cart !== null && cart !== {}) {
			await cart.map(cartItem =>
				MainActions.addToCart({ productID: cartItem.id })
			);
		}
		UserStorage.clearCart();
	}
}

export default AuthActions;
