// All the HTTP request in the application go through here
import AdminApi from "./admin";
import AuthApi from "./auth";
import MainApi from "./main";
export { AdminApi, AuthApi, MainApi };
