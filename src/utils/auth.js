import { UserStorage } from "storage";

export function isLoggedIn() {
	const { token } = UserStorage;
	return token != null;
}

export function isAdmin() {
	// if (!isLoggedIn()) return false;
	const { isAdmin } = UserStorage;

	return isAdmin === "true";
}
