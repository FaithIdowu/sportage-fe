import { AdminApi } from "services";
import { errorAlert, successAlert } from "utils/notification";
class AdminAction {
	// To add a product to the database collection
	static async addToProducts({ name, price, description, image }) {
		try {
			const response = await AdminApi.addProduct({
				name,
				price,
				description,
				image
			});
			const { success, message } = response.data;
			success ? successAlert(message) : errorAlert(message);
		} catch (err) {
			console.log(`Error while adding product${err}`);
		}
	}

	static async removeProducts({ productID }) {
		try {
			const response = await AdminApi.removeProduct({ productID });
			const { success, message } = response.data;
			success ? successAlert(message) : errorAlert(message);
		} catch (err) {
			console.log(`Error while removing product ${err}`);
		}
	}

	static async getAllOrder() {
		try {
			const response = await AdminApi.getAllOrder();
			const { success, message, list } = response.data;
			success ? successAlert(message) : errorAlert(message);
			return list;
		} catch (err) {
			console.log(`Error while retrieving orders ${err}`);
		}
	}
}

export default AdminAction;
