import React from "react";
import { Link } from "react-router-dom";
import { MainActions } from "actions";
import * as ROUTES from "routes";
import { DisplayCard, Loading } from "../Reusables";
import { Button, Icon, Modal, Form } from "semantic-ui-react";
import { isLoggedIn } from "utils/auth";
import { errorAlert } from "utils/notification";

class Cart extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			cart: [],
			isLoading: false,
			pinNumber: ""
		};
	}

	componentDidMount() {
		this.getCart();
		this.setState({ isLoading: true });
	}

	componentWillReceiveProps(props) {
		const { cart } = this.state;
		if (props.cart !== cart) {
			this.getCart();
		}
	}

	handleChange = event => {
		this.setState({ [event.target.name]: event.target.value });
	};

	handlePayment = event => {
		const { pinNumber } = this.state;
		event.preventDefault();
		this.setState({ isLoading: true });
		if (pinNumber !== "") {
			this.payForCart();
		} else {
			errorAlert("Credit Card number is required");
			this.setState({ isLoading: false });
		}
	};

	handleDelete = async id => {
		this.setState({ isLoading: true });
		this.removeFromCart(id);
	};

	async getCart() {
		try {
			const { products } = await MainActions.getCart();
			this.setState({ cart: products, isLoading: false });
		} catch (err) {
			this.setState({ isLoading: false });
			console.warn(`Error while storing cart to state: ${err}`);
		}
	}

	async removeFromCart(id) {
		try {
			const productID = id;
			await MainActions.removeFromCart({ productID });
			this.getCart();
		} catch (err) {
			console.warn(`Error while removing cart from state: ${err}`);
		}
	}

	async payForCart() {
		try {
			await MainActions.payForCart();
			this.props.history.push(ROUTES.ORDER);
		} catch (err) {
			console.warn(`Error while paying for cart in state ${err}`);
		}
	}

	render() {
		const { cart, isLoading, pinNumber } = this.state;
		const price = cart.reduce((total, cart) => {
			return total + cart.price;
		}, 0);
		if (isLoading) {
			return <Loading />;
		}
		return (
			<div>
				{cart.length > 0 ? (
					<div>
						<div className="app-leaderboard align-center flat">
							<h1>
								You have {cart.length} item(s) in your cart: ₦{price}
							</h1>
							{isLoggedIn() ? (
								<Modal
									trigger={<Button size="big">Checkout</Button>}
									size="tiny"
								>
									<Modal.Header>Payment</Modal.Header>
									<Modal.Content>
										<Form>
											<Form.Input
												label="Enter a dummy credit card pin"
												type="tel"
												value={pinNumber}
												onChange={this.handleChange}
												name="pinNumber"
												icon="vcard"
												iconPosition="left"
											/>
											<Button size="big" onClick={this.handlePayment}>
												Pay
											</Button>
										</Form>
									</Modal.Content>
								</Modal>
							) : (
								<Link to={ROUTES.AUTH}>
									<Button size="big">SignIn & Pay</Button>
								</Link>
							)}
							<br />
							<p>
								<i>Free standard shipping and free returns on all orders</i>
							</p>
						</div>
						<div className="padded">
							<div className="app-cart-card">
								<DisplayCard
									productList={cart}
									action={this.handleDelete}
									buttonText={<Icon name="x" />}
								/>
							</div>
						</div>
					</div>
				) : (
					<div className="align-center flat padded empty-container">
						<h1>Your cart is empty</h1>
						<p>(You're missing out on a lot.)</p>
						<Link to={ROUTES.HOME}>
							<Button size="big">Start shopping now!!!</Button>
						</Link>
					</div>
				)}
			</div>
		);
	}
}

export default Cart;
