import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isAdmin } from "utils/auth";
import DefaultLayout from "layouts/DefaultLayout";
import * as ROUTES from "../routes";

const ProtectedRoute = ({ component: Component, ...rest }) => {
	return (
		<Route
			{...rest}
			render={props =>
				isAdmin() ? (
					<DefaultLayout {...rest} component={Component} />
				) : (
					<Redirect
						to={{
							pathname: ROUTES.HOME,
							state: { from: props.location, isRedirect: true }
						}}
					/>
				)
			}
		/>
	);
};

export default ProtectedRoute;
