import React from "react";
import { Link } from "react-router-dom";
import * as ROUTES from "routes";

import { Menu, Icon, Image, Sidebar, MenuItem } from "semantic-ui-react";
import { isLoggedIn, isAdmin } from "utils/auth";

import crownSvg from "assets/svgs/crown.svg";

class Header extends React.Component {
	state = { visible: false };
	handleHideClick = () => this.setState({ visible: false });
	handleShowClick = () => this.setState({ visible: true });
	handleSidebarHide = () => this.setState({ visible: false });

	logout = () => {
		const { history } = this.props;
		sessionStorage.clear();
		history.push(ROUTES.HOME);
	};

	render() {
		const { visible } = this.state;
		return (
			<Menu icon="labeled" vertical borderless>
				<div className="align-top">
					{isAdmin() ? (
						<Link to={ROUTES.ADMIN}>
							<Menu.Item>
								<Image src={crownSvg} alt="" width="40px" height="40px" />
							</Menu.Item>
						</Link>
					) : (
						<Link to={ROUTES.HOME}>
							<Menu.Item>
								<Image src={crownSvg} alt="" width="40px" height="40px" />
							</Menu.Item>
						</Link>
					)}
				</div>

				<div className="align-center">
					<Menu.Item name="video camera" onClick={this.handleShowClick}>
						<Icon name="bars" />
					</Menu.Item>

					<Sidebar
						as={Menu}
						animation="overlay"
						onHide={this.handleSidebarHide}
						visible={visible}
						width="very wide"
					>
						<div className="app-logo">
							{isAdmin() ? (
								<Link
									to={ROUTES.ADMIN}
									onClick={this.handleHideClick}
									style={{ display: "flex" }}
								>
									<Menu.Item>
										<Image src={crownSvg} alt="" width="40px" height="40px" />
									</Menu.Item>
								</Link>
							) : (
								<Link
									to={ROUTES.HOME}
									onClick={this.handleHideClick}
									style={{ display: "flex" }}
								>
									<Menu.Item>
										<Image src={crownSvg} alt="" width="40px" height="40px" />
									</Menu.Item>
								</Link>
							)}
						</div>
						<div className="app-routes">
							{isAdmin() ? (
								<React.Fragment>
									<Menu.Item>
										<Link to={ROUTES.ADMIN} onClick={this.handleHideClick}>
											<h3>Products</h3>
										</Link>
									</Menu.Item>
									<Menu.Item>
										<Link to={ROUTES.ORDERS} onClick={this.handleHideClick}>
											<h3>Customer Orders</h3>
										</Link>
									</Menu.Item>
								</React.Fragment>
							) : (
								<React.Fragment>
									<Menu.Item>
										<Link to={ROUTES.HOME} onClick={this.handleHideClick}>
											<h3>Products</h3>
										</Link>
									</Menu.Item>
									<Menu.Item>
										<Link to={ROUTES.CART} onClick={this.handleHideClick}>
											<h3>Cart</h3>
										</Link>
									</Menu.Item>
									{isLoggedIn() && (
										<React.Fragment>
											<Menu.Item>
												<Link to={ROUTES.ORDER} onClick={this.handleHideClick}>
													<h3>Order</h3>
												</Link>
											</Menu.Item>
										</React.Fragment>
									)}
								</React.Fragment>
							)}

							{isLoggedIn() ? (
								<Menu.Item>
									<Link to={ROUTES.AUTH} onClick={this.logout}>
										<h3>Logout</h3>
									</Link>
								</Menu.Item>
							) : (
								<Menu.Item>
									<Link to={ROUTES.AUTH} onClick={this.handleHideClick}>
										<h3>Login / Register</h3>
									</Link>
								</Menu.Item>
							)}
						</div>

						<div className="app-social-links horizontal">
							<MenuItem>
								<a
									href="https://github.com/seyidayo"
									rel="noopener noreferrer"
									target="_blank"
								>
									<Icon color="black" size="large" name="github square" />
								</a>
							</MenuItem>
							<MenuItem>
								<a
									href="https://bitbucket.org/FaithIdowu/sportage-backend"
									rel="noopener noreferrer"
									target="_blank"
								>
									<Icon name="bitbucket square" color="black" size="large" />
								</a>
							</MenuItem>
							<MenuItem>
								<a
									href="https://twitter.com/faithidowu_"
									rel="noopener noreferrer"
									target="_blank"
								>
									<Icon name="twitter square" color="black" size="large" />
								</a>
							</MenuItem>
							<MenuItem>
								<a
									href="https://linkedin.com/in/faithidowu"
									rel="noopener noreferrer"
									target="_blank"
								>
									<Icon name="linkedin square" color="black" size="large" />
								</a>
							</MenuItem>
						</div>
					</Sidebar>
				</div>

				<div className="align-bottom">
					<div>
						<MenuItem>
							<a
								href="https://github.com/seyidayo"
								rel="noopener noreferrer"
								target="_blank"
							>
								<Icon color="black" size="large" name="github square" />
							</a>
						</MenuItem>
						<MenuItem>
							<a
								href="https://bitbucket.org/FaithIdowu/sportage-backend"
								rel="noopener noreferrer"
								target="_blank"
							>
								<Icon name="bitbucket square" color="black" size="large" />
							</a>
						</MenuItem>
						<MenuItem>
							<a
								href="https://twitter.com/faithidowu_"
								rel="noopener noreferrer"
								target="_blank"
							>
								<Icon name="twitter square" color="black" size="large" />
							</a>
						</MenuItem>
						<MenuItem>
							<a
								href="https://linkedin.com/faithidowu_"
								rel="noopener noreferrer"
								target="_blank"
							>
								<Icon name="linkedin square" color="black" size="large" />
							</a>
						</MenuItem>
					</div>
				</div>
			</Menu>
		);
	}
}

export default Header;
