/**
 * All the endpoints used to interact with the
 * backend
 */
// Authentication
export const SIGNUP = "/user/signup";
export const USER_SIGNIN = "/user/signin";
export const ADMIN_SIGNIN = "";
export const SIGNOUT = "/user/signout";

// Products
export const FETCH_ALL_PRODUCTS = "/product/list";
export const FETCH_PRODUCT_WITH_ID = "/product/list";

//Cart
export const ADD_PRODUCT_TO_CART = "/cart/add";
export const REMOVE_PRODUCT_FROM_CART = "/cart/remove";
export const FETCH_CART = "/cart/list";

//Admin
export const ADD_PRODUCT = "/product/add";
export const REMOVE_PRODUCT = "/product/remove";
export const FETCH_ORDER = "/order/list";

//Order
export const PAY_FOR_CART = "/order/add";
