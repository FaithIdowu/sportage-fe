import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";

import DefaultLayout from "./layouts/DefaultLayout";
import PrivateRoute from "./middlewares/PrivateRoute";
import ProtectedRoute from "./middlewares/ProtectedRoute";

import Admin from "./pages/Admin";
import Auth from "./pages/Auth";
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Order from "./pages/Order";
import NotFound from "./pages/NotFound";

import * as ROUTES from "./routes";

import { toast } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";
import "semantic-ui-css/semantic.min.css";
import "./assets/styles/main.scss";

toast.configure();

const App = () => {
	return (
		<BrowserRouter>
			<Switch>
				<DefaultLayout exact path={ROUTES.HOME} component={Home} />
				<DefaultLayout path={ROUTES.AUTH} component={Auth} />
				<DefaultLayout exact path={ROUTES.CART} component={Cart} />
				<PrivateRoute exact path={ROUTES.ORDER} component={Order} />
				<ProtectedRoute path={ROUTES.ADMIN} component={Admin} />
				<DefaultLayout component={NotFound} />
			</Switch>
		</BrowserRouter>
	);
};

export default App;
