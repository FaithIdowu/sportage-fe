import React from "react";
import { Button, Card, Image } from "semantic-ui-react";

export const DisplayCard = ({ productList, action, buttonText }) => {
	return productList.map((product, index) => (
		<Card key={index}>
			<Image src={product.image} alt={product.name} wrapped />
			<Card.Content>
				<Card.Header>{product.name}</Card.Header>
				<Card.Meta>
					<span>₦{product.price}</span>
				</Card.Meta>
				<Card.Description>{product.description}</Card.Description>
			</Card.Content>
			{action ? (
				<Card.Content extra>
					<Button size="medium" onClick={e => action(product.id)}>
						{buttonText}
					</Button>
				</Card.Content>
			) : null}
		</Card>
	));
};

export const OrderDisplayCard = ({ orderList }) => {
	return orderList.map((order, index) => (
		<Card key={index} fluid>
			<Card.Content>
				<Card.Header>
					{order.owner.firstName} {order.owner.lastName}
				</Card.Header>
				<Card.Meta>
					{order.products.map((product, index) => (
						<p key={index}>
							{product.name}: ₦{product.price}
						</p>
					))}
				</Card.Meta>
				<Card.Description>
					<p>
						Delivery Address: <strong>{order.owner.address}</strong>
					</p>
					<p>
						Phone Number: <strong>{order.owner.phoneNumber}</strong>
					</p>
					<p>
						Email: <strong>{order.owner.email}</strong>
					</p>
				</Card.Description>
			</Card.Content>
		</Card>
	));
};
