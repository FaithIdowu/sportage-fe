import React from "react";
import { Link } from "react-router-dom";
import { Loading } from "components/Reusables";

import { AuthActions } from "actions";
import { HOME, AUTH } from "routes";
import { Button, Form, Message, Segment } from "semantic-ui-react";
import { errorAlert, successAlert } from "utils/notification";

class SignUp extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			firstName: "",
			lastName: "",
			email: "",
			phoneNumber: "",
			address: "",
			password: "",

			isLoading: false
		};
	}

	handleChange = event => {
		this.setState({ [event.target.name]: event.target.value });
	};

	handleSubmit = event => {
		event.preventDefault();
		this.setState({ isLoading: true });
		this.signup();
	};

	get formIncomplete() {
		const {
			firstName,
			lastName,
			email,
			address,
			phoneNumber,
			password
		} = this.state;
		return (
			firstName === "" ||
			lastName === "" ||
			email === "" ||
			address === "" ||
			phoneNumber === "" ||
			password === ""
		);
	}

	async signup() {
		if (this.formIncomplete) {
			return errorAlert("Please fill in your details");
		}
		try {
			const { history } = this.props;
			const { message, success } = await AuthActions.signup({ ...this.state });

			if (success) {
				history.push(HOME);
				successAlert(message);
			} else {
				errorAlert(message);
				this.setState({ isLoading: false });
			}
		} catch (err) {
			this.setState({ isLoading: false });
		}
	}

	render() {
		const { isLoading } = this.state;
		if (isLoading) {
			return <Loading />;
		}
		return (
			<React.Fragment>
				<Segment>
					<h3 className="mb">Create your new account</h3>
					<Form onSubmit={this.handleSubmit}>
						<Form.Input
							label="First Name"
							icon="user"
							iconPosition="left"
							type="text"
							name="firstName"
							onChange={this.handleChange}
							placeholder="First Name"
							fluid
							size="large"
							required
							focus
						/>

						<Form.Input
							label="Last Name"
							size="large"
							icon="users"
							iconPosition="left"
							type="text"
							name="lastName"
							onChange={this.handleChange}
							placeholder="Last Name"
							required
						/>

						<Form.Input
							label="Email Address"
							icon="mail"
							iconPosition="left"
							type="email"
							name="email"
							onChange={this.handleChange}
							placeholder="jon@snow.com"
							size="large"
							required
						/>

						<Form.Input
							icon="phone"
							iconPosition="left"
							type="tel"
							name="phoneNumber"
							onChange={this.handleChange}
							placeholder="+234 703 393 4567"
							label="Phone Number"
							size="large"
							required
						/>

						<Form.Input
							label="Delivery Address"
							icon="home"
							iconPosition="left"
							type="text"
							name="address"
							onChange={this.handleChange}
							placeholder="Castle Black"
							size="large"
							required
						/>
						<Form.Input
							icon="lock"
							label="Password"
							iconPosition="left"
							type="password"
							name="password"
							onChange={this.handleChange}
							placeholder="Password"
							size="large"
							required
						/>

						<Button
							fluid
							type="submit"
							size="large"
							disabled={this.state.isLoading}
						>
							Sign up
						</Button>
					</Form>
				</Segment>
				<Message size="large">
					<p className="text-center">
						Already have an account? <Link to={AUTH}>Login here</Link>
					</p>
				</Message>
			</React.Fragment>
		);
	}
}

export default SignUp;
