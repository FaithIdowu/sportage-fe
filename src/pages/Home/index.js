import React from "react";
import Main from "components/MainComponents";

export default () => (
	<div>
		<div className="app-banner flat">
			<h1>Sportage</h1>
			<p>
				shoes made for the <span className="app-affirmation">King</span> in you
			</p>
			<div className="app-banner-bg" />
		</div>
		<div className="app-container padded">
			<Main />
		</div>
	</div>
);
