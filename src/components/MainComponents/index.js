import React from "react";
import { Link } from "react-router-dom";
import { MainActions } from "actions";
import { DisplayCard, Loading } from "../Reusables";
import { Button, Card, Header, Icon } from "semantic-ui-react";

import { isAdmin } from "utils/auth";
import { ADMIN } from "routes";
class Main extends React.Component {
	constructor() {
		super();

		this.state = {
			products: [],
			isLoading: false
		};
	}

	componentDidMount() {
		this.setState({ isLoading: true });
		this.getProducts();
	}

	handleAdd = id => {
		this.setState({ isLoading: true });
		this.addProductToCart(id);
	};

	async getProducts() {
		const products = await MainActions.getProducts();
		this.setState({ products, isLoading: false });
	}

	async addProductToCart(id) {
		try {
			const productID = id;
			await MainActions.addToCart({ productID });
			this.setState({ isLoading: false });
		} catch (err) {
			throw err;
		}
	}

	render() {
		const { products, isLoading } = this.state;
		if (isLoading) {
			return <Loading />;
		}
		if (isAdmin()) {
			return (
				<div className="align-center">
					<Link to={ADMIN}>
						<Button size="huge">
							<Icon name="plus" />
							Add Products
						</Button>
					</Link>
				</div>
			);
		}
		return (
			<React.Fragment>
				{products.length > 0 ? (
					<div>
						<Card.Group stackable items centered>
							<DisplayCard
								productList={products}
								action={this.handleAdd}
								buttonText="Add to cart"
							/>
						</Card.Group>
					</div>
				) : (
					<div className="align-center flat">
						<Header size="huge">Products cannot be shown now</Header>
						<p>(There's probably something wrong with Faith.)</p>
					</div>
				)}
			</React.Fragment>
		);
	}
}

export default Main;
