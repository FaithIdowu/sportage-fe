import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";
export const Loading = () => {
	return (
		<Dimmer active inverted>
			<Loader size="large">Fulfilling Your Command</Loader>
		</Dimmer>
	);
};
