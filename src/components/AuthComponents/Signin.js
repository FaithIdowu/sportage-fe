import React from "react";
import { Link } from "react-router-dom";
import { AuthActions } from "actions";
import { Loading } from "components/Reusables";
import { HOME, SIGNUP } from "routes";
import { successAlert, errorAlert } from "utils/notification";
import { Button, Form, Message, Segment } from "semantic-ui-react";

class SignIn extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			email: "",
			password: "",
			isLoading: false
		};
	}

	handleChange = event => {
		this.setState({ [event.target.name]: event.target.value });
	};

	handleSubmit = event => {
		event.preventDefault();
		this.setState({ isLoading: true });
		this.signin();
	};

	get formIncomplete() {
		const { email, password } = this.state;
		return email === "" || password === "";
	}

	async signin() {
		if (this.formIncomplete) {
			const errorMsg = "Please Complete your credentials";
			this.setState({ isLoading: false });
			return errorAlert(errorMsg);
		}
		try {
			const { history } = this.props;
			const { message, success } = await AuthActions.signin({ ...this.state });
			if (success) {
				history.push(HOME);
				successAlert(message);
			} else {
				errorAlert(message);
				this.setState({ isLoading: false });
			}
		} catch (err) {
			this.setState({ isLoading: false });
		}
	}

	render() {
		const { isLoading } = this.state;
		if (isLoading) {
			return <Loading />;
		}
		return (
			<React.Fragment>
				<Segment>
					<h3 className="mb">Login in to your account</h3>
					<Form onSubmit={this.handleSubmit}>
						<Form.Input
							icon="user"
							iconPosition="left"
							type="email"
							name="email"
							onChange={this.handleChange}
							placeholder="Email Address"
							size="large"
							label="Email Address"
							focus
						/>
						<Form.Input
							icon="lock"
							iconPosition="left"
							type="password"
							name="password"
							onChange={this.handleChange}
							placeholder="Password"
							size="large"
							label="Password"
						/>
						<Button
							fluid
							type="submit"
							size="large"
							disabled={this.state.isLoading}
						>
							Login
						</Button>
					</Form>
				</Segment>
				<Message size="large">
					<p className="text-center">
						Don't have an account? <Link to={SIGNUP}>Get Started</Link>
					</p>
				</Message>
			</React.Fragment>
		);
	}
}

export default SignIn;
