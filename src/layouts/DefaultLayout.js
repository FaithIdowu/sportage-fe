import React from "react";
import { Route } from "react-router-dom";

import Header from "./components/Header";
import Footer from "./components/Footer";

const DefaultLayout = ({ component: Component, ...rest }) => {
	return (
		<Route
			{...rest}
			render={matchProps => (
				<div className="app-wrapper">
					<div className="app-header">
						<Header {...matchProps} />
					</div>

					<div className="app-body">
						<Component {...matchProps} />
					</div>

					<div className="app-footer">
						<Footer />
					</div>
				</div>
			)}
		/>
	);
};
export default DefaultLayout;
