import axios from "axios";

export const HTTP = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
	headers: {
		"Content-Type": "application/json"
	}
});

export const generateBearer = token => {
	return { headers: { Authorization: `Bearer ${token}` } };
};

export const generateMultipartHeader = () => {
	return { headers: { "Content-Type": `multipart/form-data` } };
};

export const mergeHeaders = (...headerCollections) => {
	const result = {};
	headerCollections.map(content => {
		const { headers } = content;
		return Object.assign(result, headers);
	});

	return { headers: result };
};
