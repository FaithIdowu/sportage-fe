import React from "react";
import { AdminActions } from "actions";
import { errorAlert } from "utils/notification";
import { Button, Form } from "semantic-ui-react";
import { Loading } from "components/Reusables";

class AddProduct extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			name: "",
			price: "",
			description: "",
			isLoading: false
		};
		this.fileInput = React.createRef();
	}

	handleChange = event => {
		this.setState({ [event.target.name]: event.target.value });
	};

	handleSubmit = event => {
		event.preventDefault();
		this.setState({ isLoading: true });
		this.addProduct();
	};

	get formIncomplete() {
		const { name, price, description } = this.state;
		return (
			name === "" ||
			price === "" ||
			description === "" ||
			this.fileInput.current.files.length <= 0
		);
	}

	async addProduct() {
		if (this.formIncomplete) {
			const errMsg = "complete your product details";
			this.setState({ isLoading: false });
			return errorAlert(errMsg);
		}
		try {
			const fileArray = this.fileInput.current.files;
			await AdminActions.addToProducts({
				...this.state,
				image: fileArray[0]
			});
			this.setState({ isLoading: false });
		} catch (err) {
			this.setState({ isLoading: false });
		}
	}

	render() {
		if (this.state.isLoading) {
			return <Loading />;
		}
		return (
			<Form onSubmit={this.handleSubmit}>
				<Form.Input
					label="Product Name"
					iconPosition="left"
					icon="gift"
					type="text"
					name="name"
					onChange={this.handleChange}
					placeholder="Nike Epic React"
					size="big"
					required
				/>

				<Form.Input
					label="Product Price"
					iconPosition="left"
					icon="dollar"
					type="number"
					name="price"
					onChange={this.handleChange}
					placeholder="300,000"
					size="big"
					required
				/>

				<Form.Input
					label="Product Description"
					iconPosition="left"
					icon="pin"
					type="text"
					name="description"
					onChange={this.handleChange}
					placeholder="The shoe for angels"
					size="big"
					required
				/>

				<input type="file" ref={this.fileInput} accept="images*/" required />

				<Button onClick={this.handleSubmit}>Add product</Button>
			</Form>
		);
	}
}

export default AddProduct;
