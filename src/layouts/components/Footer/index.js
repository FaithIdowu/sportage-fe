import React from "react";
import { Menu, Icon, Image } from "semantic-ui-react";
import { isLoggedIn } from "utils/auth";

import stampSvg from "assets/svgs/circular-label-with-certified-stamp.svg";

export default () => (
	<Menu icon="labeled" vertical borderless>
		<div className="align-top ">
			<Menu.Item>
				<Image src={stampSvg} alt="" width="40px" height="40px" />
			</Menu.Item>
		</div>

		<div className="align-center ">
			{isLoggedIn() ? (
				<Menu.Item>
					<Icon name="lock open" />
				</Menu.Item>
			) : (
				<Menu.Item>
					<Icon name="lock" />
				</Menu.Item>
			)}
		</div>

		<div className="align-bottom ">
			<Menu.Item>
				<h2 style={{ transform: "rotate(-90deg)", marginTop: "60px" }}>
					Sportage
				</h2>
			</Menu.Item>
		</div>
	</Menu>
);
