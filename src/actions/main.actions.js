import { MainApi } from "services";
import { UserStorage } from "storage";
import { isLoggedIn } from "utils/auth";
import { successAlert, errorAlert } from "utils/notification";

// TODO: create a toast to circulate errors

class MainAction {
	// To fetch the products
	static async getProducts() {
		try {
			const response = await MainApi.getProducts();
			return response ? response.data.list : [];
		} catch (err) {
			console.log(`Error while retrieving product${err}`);
			return {};
		}
	}

	// To fetch a single product using its ID
	static async getProductWithID({ productID }) {
		try {
			const response = await MainApi.getProductWithID({ productID });
			return response.data.product;
		} catch (err) {
			console.log(`Error while retrieving a particular product${err}`);
		}
	}

	/**
	 * To add a product to the users cart
	 * When logged in products are stored on a cart collection in the database
	 * otherwise products are stored in localStorage
	 */

	static async addToCart({ productID }) {
		let cart = [];
		try {
			const product = await this.getProductWithID({ productID });
			var newItem = product;
			if (isLoggedIn()) {
				const userID = await UserStorage.userID;
				const response = await MainApi.addToCart({ productID, userID });
				const { success, message } = response.data;
				success ? successAlert(message) : errorAlert(message);
			} else {
				let oldItems = await UserStorage.getCart();
				if (oldItems === null) {
					cart.push(newItem);
				} else {
					cart.push(...oldItems);
					cart.push(newItem);
				}
				successAlert("Added to cart");
				UserStorage.createCart(cart);
			}
		} catch (err) {
			console.log(`Error while adding to cart ${err}`);
		}
	}

	/**
	 * To fetch a users cart
	 * When logged in cart is fetch from database collection
	 * otherwise products are fetched from localStorage
	 */
	static async getCart() {
		try {
			if (isLoggedIn()) {
				const userID = UserStorage.userID;
				const response = await MainApi.getCart({ userID });
				return response.data.list[0];
			} else {
				const products = await UserStorage.getCart();
				return products !== null ? { products } : { products: {} };
			}
		} catch (err) {
			console.log(`Error while retrieving cart${err}`);
		}
	}

	/**
	 * To remove a product to the users cart
	 * When logged in products are deleted cart collection in database
	 * otherwise products are sliced out from cart in localStorage
	 */
	static async removeFromCart({ productID }) {
		try {
			if (isLoggedIn()) {
				const userID = await UserStorage.userID;
				const response = await MainApi.removeFromCart({
					productID,
					userID
				});
				const { success, message } = response.data;
				success ? successAlert(message) : errorAlert(message);
			} else {
				let cart = await UserStorage.getCart();
				const cartItem = cart.findIndex(cartItem => cartItem.id === productID);
				const newCart = [
					...cart.slice(0, cartItem),
					...cart.slice(cartItem + 1)
				];
				let response = { message: "Removed from cart", success: true };
				UserStorage.createCart(newCart);
				return response;
			}
		} catch (err) {
			console.log(`Error while removing item from cart${err}`);
		}
	}

	// To pay for the items in a users cart
	static async payForCart() {
		try {
			const userID = UserStorage.userID;
			const response = await MainApi.payForCart({ userID });
			const { success, message } = response.data;
			success ? successAlert(message) : errorAlert(message);
		} catch (err) {
			console.log(`Error while paying for cart ${err}`);
		}
	}

	// To fetch a users order list
	static async getOrder() {
		try {
			const userID = await UserStorage.userID;
			const response = await MainApi.getOrder({ userID });
			return response.data.list[0];
		} catch (err) {
			console.log(`Error while retrieving user's order ${err}`);
			return {};
		}
	}
}

export default MainAction;
