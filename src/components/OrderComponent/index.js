import React from "react";
import { Link } from "react-router-dom";

import { MainActions } from "actions";
import { DisplayCard } from "components/Reusables";

import { Button, Card } from "semantic-ui-react";

import * as ROUTES from "routes";

class Order extends React.Component {
	constructor() {
		super();
		this.state = {
			order: [],
			isLoading: false
		};
	}

	componentDidMount() {
		this.getOrder();
		this.setState({ isLoading: true });
	}

	async getOrder() {
		try {
			const { products } = await MainActions.getOrder();
			this.setState({ order: products, isLoading: false });
		} catch (err) {
			console.warn(`Error while storing orders in state: ${err}`);
		}
	}

	render() {
		const { order } = this.state;
		return (
			<div>
				{order.length > 0 ? (
					<div className="padded">
						<h3>Orders</h3>
						<Card.Group stackable items>
							<DisplayCard productList={order} />
						</Card.Group>
					</div>
				) : (
					<div className="align-center flat padded empty-container">
						<h3>You haven't made any orders yet</h3>
						<p>(You're missing out on a lot.)</p>
						<Link to={ROUTES.HOME}>
							<Button size="big">Start shopping now!!!</Button>
						</Link>
					</div>
				)}
			</div>
		);
	}
}

export default Order;
